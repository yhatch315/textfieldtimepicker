//
//  AppDelegate.h
//  TextfieldTimePicker
//
//  Created by Edward P. Legaspi on 8/1/14.
//  Copyright (c) 2014 Kalidad Biz Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
