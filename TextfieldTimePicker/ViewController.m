//
//  ViewController.m
//  TextfieldTimePicker
//
//  Created by Edward P. Legaspi on 8/1/14.
//  Copyright (c) 2014 Kalidad Biz Solutions. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
- (IBAction)btn_showTime:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *lbl_timeLabel;
@property (weak, nonatomic) IBOutlet UITextField *txtField_inputTextfield;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self setUpTextFieldDatePicker];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btn_showTime:(id)sender {
    self.lbl_timeLabel.text = self.txtField_inputTextfield.text;
    [self.txtField_inputTextfield resignFirstResponder];
}

-(void)setUpTextFieldDatePicker
{
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeTime;
    [datePicker setDate:[NSDate date]];
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    [self.txtField_inputTextfield setInputView:datePicker];
}

-(void)updateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)self.self.txtField_inputTextfield.inputView;
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"]; //24hr time format
    NSString *dateString = [outputFormatter stringFromDate:picker.date];
    
    self.txtField_inputTextfield.text = [NSString stringWithFormat:@"%@",dateString];
}



@end
